syntax on
set number
set pastetoggle=<F4>
set ignorecase
set smartcase
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
"colorscheme vividchalk
"colorscheme guardian
colorscheme desert

autocmd FileType make setlocal noexpandtab
autocmd FileType Makefile setlocal noexpandtab

" Commenting blocks of code.
autocmd FileType c,cpp,go,java,rust,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python           let b:comment_leader = '# '
autocmd FileType conf,fstab               let b:comment_leader = '# '
autocmd FileType tex                      let b:comment_leader = '% '
autocmd FileType mail                     let b:comment_leader = '> '
autocmd FileType vim                      let b:comment_leader = '" '

noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

